import * as React from 'react'
import {GoogleApiWrapper, InfoWindow, Marker, Map} from 'google-maps-react'
import './style.scss'
import {AppBar} from '../index'

export class ReportMap extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            alertas: [],
            categorias: [],
            usuarios: [],
            // markerObjects: [],
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
            fetching: false,
        }

        this.convertCategory = this.convertCategory.bind(this)
        this.convertUsuario = this.convertUsuario.bind(this)
    }

    onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    })

    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
        this.setState({
            showingInfoWindow: false,
            activeMarker: null
        })
        }
    }

    // onAlertaNoPainelClicked = (i, e) => {
    //     // let alerta = this.state.alertas[i];
    //     // let marker = this.state.markerObjects[i];
    //     this.setState({
    //         selectedPlace: (this.state.alertas)[i],
    //         activeMarker: (this.state.markerObjects)[i],
    //         showingInfoWindow: true
    //       })
    //     // this.onMarkerClick(this.state.alertas[i], this.state.markerObjects[i])
    //     alert(this.state.markerObjects[i].usuario+' '+this.state.alertas[i].data);
    //     alert(this.state.selectedPlace.data);
    //     alert(this.state.activeMarker.usuario);
    // }

    // onMarkerMounted = element => {
    //     this.setState(prevState => ({
    //         markerObjects: [...prevState.markerObjects, element.marker]
    //     }))
    // };
    
    componentDidMount(){
        this.setState({
            fetching: true,
        })
        let app = this
        setInterval(function(){
		    fetch(
		        'http://paradigmas.hol.es/web/alerta',
		        {
		            method: 'GET',
		            headers: {
		                'Content-Type':'application/json',
		                'Authorization' : 'Basic ' + btoa(localStorage.getItem("access_token") + ':')
		            }
		        }
		    ).then(
		        (response) => {
		            if(response.ok){
		                return response.json()
		            }else{
		                console.log("Error")
		                window.location.replace("/unauthorized")
		                throw new Error(response.statusText)
		            }
		        }
		    ).then(
		        (response) => {
		            app.setState({
		                alertas: response,
		                fetching: false
		            })
		        }
		    ).catch(
		        (error) => {
		            console.log(error)
		        }
		    )
		    fetch(
		        'http://paradigmas.hol.es/web/categoria',
		        {
		            method: 'GET',
		            headers: {
		                'Content-Type':'application/json',
		                'Authorization' : 'Basic ' + btoa(localStorage.getItem("access_token") + ':')
		            }
		        }
		    ).then(
		        (response) => {
		            if(response.ok){
		                return response.json()
		            }else{
		                console.log("Error")
		                throw new Error(response.statusText)
		            }
		        }
		    ).then(
		        (response) => {
		            console.log(response)
		            app.setState({
		                categorias: response
		            })
		        }
		    ).catch(
		        (error) => {
		            console.log(error)
		        }
            )
            fetch(
		        'http://paradigmas.hol.es/web/usuario',
		        {
		            method: 'GET',
		            headers: {
		                'Content-Type':'application/json',
		                'Authorization' : 'Basic ' + btoa(localStorage.getItem("access_token") + ':')
		            }
		        }
		    ).then(
		        (response) => {
		            if(response.ok){
		                return response.json()
		            }else{
		                console.log("Error")
		                throw new Error(response.statusText)
		            }
		        }
		    ).then(
		        (response) => {
		            console.log(response)
		            app.setState({
		                usuarios: response
		            })
		        }
		    ).catch(
		        (error) => {
		            console.log(error)
		        }
		    )
	    }, 5000)
        
    }

    /**verificar pq não está retornando o nome da categoria */
    convertCategory(num){
        return this.state.categorias.find((elem) => {
            if(elem.id === num){
                return true
            }else{
                return false
            }
       })
    }

    convertUsuario(num){
        return this.state.usuarios.find((elem) => {
            if(elem.id === num){
                return true
            }else{
                return false
            }
       })
    }

    renderConteudoPainelAlertas(){
        const alertas = this.state.alertas;
        const listItems = alertas.map((d, i) => 
            <li className="alerta-no-painel" key={d.id} /*onClick={(e) => this.onAlertaNoPainelClicked(i, e)}*/>
                <b>{this.convertCategory(d.categoria_id) && this.convertCategory(d.categoria_id).nome}</b>
                <br/>
                <span>{this.convertUsuario(d.usuario_id) && this.convertUsuario(d.usuario_id).nome}</span>
            </li>
        );

        return (
        <div>
            {listItems }
        </div>
        );
    }

    render(){
        console.log(this.state.categorias)
            return(
                this.state.fetching
                ? <div className="loading">Por favor espere</div>
                : <React.Fragment>
                    <AppBar />
                    <div id="floating-panel">
                        <button onClick={this.toggleHeatmap}>Mancha Criminal</button>
                    </div>
                    <div id="painel-alertas">
                        { this.renderConteudoPainelAlertas() }
                    </div>
                    <Map
                        google={this.props.google} 
                        zoom={16}
                        style={{height:'calc(100% - 64px)'}}
                        initialCenter={{
                            lat: -20.277406,
                            lng: -40.303576
                          }}
                    >
                    {this.state.alertas.map((mark,index)=>{
                        return (
                        <Marker 
                                //ref={this.onMarkerMounted}
                                //animation={this.props.google.maps.Animation.DROP}
                                name={mark.categoria_id}
                                onClick={this.onMarkerClick}
                                key={index}
                                position={{lat: mark.latitude,lng:mark.longitude}}
                                horario={mark.data}
                                usuario={mark.usuario_id}
                        />
                        )
                    })}
                    <InfoWindow
                        marker={this.state.activeMarker}
                        visible={this.state.showingInfoWindow}>
                            <div>
                            <h1>{this.convertCategory(this.state.selectedPlace.name) && this.convertCategory(this.state.selectedPlace.name).nome}</h1>
                            <h2>{this.convertUsuario(this.state.selectedPlace.usuario) && this.convertUsuario(this.state.selectedPlace.usuario).nome}</h2>
                            <h4>Horário: {this.state.selectedPlace.horario}</h4>
                            <h4>Latitude: {this.state.selectedPlace.position && this.state.selectedPlace.position.lat}</h4>
                            <h4>Longitude: {this.state.selectedPlace.position && this.state.selectedPlace.position.lng}</h4>
                            </div>
                    </InfoWindow>
                    </Map>
                </React.Fragment>
                )
        }

    toggleHeatmap() {
        // var heatmap = new google.maps.visualization.HeatmapLayer({
        //     data: [
        //         new google.maps.LatLng(-20.277741, -40.305871)
        //     ],
        //     map: Map
        // });
        // heatmap.setMap(heatmap.getMap() ? null : Map);
        alert('Mancha criminal')
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyCWEnXbHjT-ISx14MCXbeQ755i4k7QISU0")
  })(ReportMap)
