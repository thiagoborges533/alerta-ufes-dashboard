import * as React from 'react'
import './style.scss'

export default class PopList extends React.Component{
    render(){
        return (
            <div className="poplist">
                {this.props.children}
            </div>
        )
    }
}