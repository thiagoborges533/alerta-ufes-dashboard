import * as React from 'react'
import {Link} from 'react-router-dom'
import './style.scss'


export default class AppBar extends React.Component{

    logout(){
        window.localStorage.clear()
    }


    render(){
        return(
            <div className="appbar">
                <span>
                    <img className="img" src="../../../images/logo-sem-nome.svg" alt="logo" />
                </span>
                <span className="title">
                    <span className="white-text">
                        ALERTA
                    </span>
                    <span className="pink-text">
                        UFES
                    </span>
                </span>
                <ul className="menu">
                    <li>
                        <Link to="/alertas">Mapa</Link>
                    </li>
                    <li>
                        <Link to="/ocorrencias">Ocorrências</Link>
                    </li>
                    <li>
                        <Link to="/usuarios">Usuários</Link>
                    </li>
                    <li>
                        <a href="/" onClick={this.logout}>Sair</a>
                    </li>
                </ul>
            </div>
        )
    }
}