import * as React from 'react'
import './style.scss'

export default class ListItem extends React.Component{
    render(){
        return(
            <div className="list-item">
                <div className="list-item--main">
                    {this.props.main_info}
                </div>
                <div className="list-item--other">
                    <span className="list-item--other-text">
                        {this.props.sec_info}
                    </span>
                    <span className="list-item--other-text">
                        {this.props.trd_info}
                    </span>
                </div>
            </div>
        )
    }
}