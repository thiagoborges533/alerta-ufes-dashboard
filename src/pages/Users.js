import * as React from 'react'
import {AppBar, PopList, ListItem} from '../components'

export default class Users extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            usuarios: [],
            fetching: false
        }
    }

    componentDidMount(){
        this.setState({
            fetching:true
        })
        fetch(
            'http://paradigmas.hol.es/web/usuario',
            {
                method: 'GET',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization' : 'Basic ' + btoa(localStorage.getItem("access_token") + ':')
                }
            }
        ).then(
            (response) => {
                if(response.ok){
                    return response.json()
                }else{
                    console.log("Error")
                    window.location.replace("/unauthorized")
                    throw new Error(response.statusText)
                }
            }
        ).then(
            (response) => {
                console.log(response)
                this.setState({
                    usuarios: response,
                    fetching: false
                })
            }
        ).catch(
            (error) => {
                console.log(error)
            }
        )
    }

    render(){
        return(
            this.state.fetching
            ? <div className="loading">Por favor espere</div>
            : <div>
                <AppBar />
                <h2 className="header">Usuários</h2>
                <PopList>
                    {this.state.usuarios.map((user,index)=>{
                        return(
                            <ListItem
                                key={index}
                                main_info={"Nome: " + user.nome}
                                sec_info={"Telefone: " + user.telefone}
                                trd_info={"Token: " + user.access_token}
                            />
                        )
                    })}
                </PopList>
            </div>
        )
    }
}