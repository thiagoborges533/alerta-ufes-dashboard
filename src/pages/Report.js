import * as React from 'react'
import * as Components from '../components'

export default class Report extends React.Component{
    render(){
        return(
            <React.Fragment>
                <Components.ReportMap />
            </React.Fragment>
        )
    }
}