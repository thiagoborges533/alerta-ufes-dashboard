import * as React from 'react'
import {AppBar, PopList, ListItem} from '../components'
import '../style/occur.scss'

export default class Occurrences extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            alertas: [],
            categoria: 0,
            categorias: [],
            fetching: false
        }
        this.handleCategory = this.handleCategory.bind(this)
        this.convertCategory = this.convertCategory.bind(this)
    }

    componentDidMount(){
        this.setState({
            fetching: true
        })
        fetch(
            'http://paradigmas.hol.es/web/alerta',
            {
                method: 'GET',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization' : 'Basic ' + btoa(localStorage.getItem("access_token") + ':')
                }
            }
        ).then(
            (response) => {
                if(response.ok){
                    return response.json()
                }else{
                    console.log("Error")
                    window.location.replace("/unauthorized")
                    throw new Error(response.statusText)
                }
            }
        ).then(
            (response) => {
                console.log(response)
                this.setState({
                    alertas: response
                })
            }
        ).catch(
            (error) => {
                console.log(error)
            }
        )
        fetch(
            'http://paradigmas.hol.es/web/categoria',
            {
                method: 'GET',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization' : 'Basic ' + btoa(localStorage.getItem("access_token") + ':')
                }
            }
        ).then(
            (response) => {
                if(response.ok){
                    return response.json()
                }else{
                    console.log("Error")
                    throw new Error(response.statusText)
                }
            }
        ).then(
            (response) => {
                console.log(response)
                this.setState({
                    categorias: response,
                    fetching: false
                })
            }
        ).catch(
            (error) => {
                console.log(error)
            }
        )
    }

    convertCategory(num){
        console.log(this.state.categorias)
        return this.state.categorias.find((elem) => {
            if(elem.id === num){
                return true
            }else{
                return false
            }
       })
    }

    handleCategory(e){
        e.preventDefault()
        e.stopPropagation()
        const actualCat = this.state.categorias.find((elem) => {
            return elem.nome === e.target.selectedOptions[0].innerText
        })
        console.log(actualCat)
        if(!actualCat){
            this.setState({
                categoria: 0
            })
        }else{
            this.setState({
                categoria: actualCat.id
            })
        }
    }

    render(){
        return(
            this.state.fetching
            ? <div className="loading">Por favor espere</div>
            : <div>
                <AppBar />
                <div>
                    <h2 className="header">Ocorrencias</h2>
                    <div className="occur--select-container">
                        Categorias: <select className="occur--select" onChange={this.handleCategory} defaultValue={this.state.categoria}>
                        <option value="all">Todas</option>
                        {this.state.categorias.map((cat, index)=>{
                            return <option value={cat} key={index}>{cat.nome}</option>
                        })}
                    </select>
                    </div>
                    <PopList>
                        {this.state.categoria === 0
                        ? 
                        this.state.alertas.map((alert,index) => {
                            return (
                                <ListItem 
                                    key={index} 
                                    main_info={"ID: " + alert.id} 
                                    sec_info={"Localização: " + alert.longitude + ", " + alert.latitude} 
                                />
                            )
                        })

                        :
                        this.state.alertas.filter((alerta,index) => {
                            return alerta.categoria_id === this.state.categoria
                        }).map((alert,index) => {
                            return(
                            <ListItem 
                                key={index} 
                                main_info={"ID: " + 
                                alert.id} 
                                sec_info={"Latitude: " + alert.latitude} 
                                trd_info={"Longitude: " + alert.longitude}
                            />
                                )
                            })
                        }
                    </PopList>
                </div>
            </div>
        )
    }
}