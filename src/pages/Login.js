import * as React from 'react'
import '../style/login.scss'

export default class Login extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            fetching: false,
            error: false
        }

        this.fetchLogin = this.fetchLogin.bind(this)
    }

    fetchLogin(event){
        event.preventDefault()
        event.stopPropagation()
        this.setState({
            fetching: true
        })
        fetch(
            'http://paradigmas.hol.es/web/usuario/login',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body:
                    JSON.stringify({
                        login: event.target.elements["login_input"].value,
                        senha: event.target.elements["psw_input"].value
                    }),
            }
        ).then(
            (response) => {
                if(response.ok){
                    return response.json()
                }else{
                    console.log(response)
                    this.setState({
                        fetching: false,
                        error: true
                    })
                    throw new Error(response.statusText)
                }
            }
        ).then(
            (response) => {
                localStorage.setItem('access_token', response.access_token)
                localStorage.setItem('nome_usuario', response.nome)
                localStorage.setItem('id_usuario', response.id)
                setTimeout(window.location.replace("/alertas"),100) // é uma gambiarra muito feia, mas por enquanto, ta funcionando
                this.setState({
                    fetching: false,
                    error: false
                })
            }
        ).catch(
            (error) => {
                console.log(error)
            }
        )
    }

    render(){
        return(
            <div className="login">
                <div className="login_input">
                <img className="login_input--logo" alt="logo alerta-ufes" src="/images/logo.svg" />
                    <div  className="login_input--form">
                        <form onSubmit={this.fetchLogin}>
                            {this.state.error && <div className="login_input--error">Tente novamente</div>}
                            <div>
                                <input className="login_input--field" name="login" id="login_input" placeholder="usuário" type="text"/>
                            </div>
                            <div>
                                <input className="login_input--field" name="psw" id="psw_input" placeholder="senha" type="password"/>
                            </div>
                            <div>
                                <button className="login_input--button" type="submit" disabled={this.state.fetching}>Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
