import * as React from 'react'
import '../style/notfound.scss'

export default function NotFound(){
    return (
        <div className="notfound">
            <img className="notfound--logo" alt="logo alerta-ufes" src="/images/logo.svg" />
            <div className="notfound--msg">
                Página não encontrada
            </div>
        </div>
    )
}