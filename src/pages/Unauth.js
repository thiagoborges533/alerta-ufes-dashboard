import * as React from 'react'
import '../style/unauth.scss'

export default function Unauth(){
    return (
        <div className="unauth">
            <img className="unauth--logo" alt="logo alerta-ufes" src="/images/logo.svg" />
            <div className="unauth--msg">
                Você não está autorizado
            </div>
        </div>
    )
}