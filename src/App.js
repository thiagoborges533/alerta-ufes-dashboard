import React, { Component } from 'react'
import {BrowserRouter,Route, Switch} from 'react-router-dom'
import * as Pages from './pages'
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Pages.Login} />
            <Route exact path="/alertas" render={() =>
              <React.Fragment>
                <Pages.Report />
              </React.Fragment>
            } />
            <Route exact path="/ocorrencias" render={() =>
              <React.Fragment>
                <Pages.Occurrences />
              </React.Fragment>
            } />
            <Route exact path="/usuarios" render={() =>
              <React.Fragment>
                <Pages.Users />
              </React.Fragment>
            } />
            <Route exact path="/unauthorized" render={() =>
              <React.Fragment>
                <Pages.Unauth />
              </React.Fragment>
            } />
            <Route render={() =>
              <Pages.NotFound />
          } />
          </Switch>
        </BrowserRouter>
      </React.Fragment>
    );
  }
}

export default App;
